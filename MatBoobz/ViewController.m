//
//  ViewController.m
//  MatBoobz
//
//  Created by Kirill Simagin on 21/11/12.
//  Copyright (c) 2012 Kirill Simagin. All rights reserved.
//

#import "ViewController.h"
#import "BoobzView.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    BoobzView * boobzy = [[BoobzView alloc] initWithFrame:CGRectMake(100, 100, 420, 228)];
    [boobzy setBackgroundColor:[UIColor whiteColor]];
    [[self view] addSubview:boobzy];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//MARK: rotation management

// !!!: deprecated in iOS 6 => needed for support <iOS5

-(BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

-(NSInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}
@end
