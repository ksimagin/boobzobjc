//
//  AppDelegate.h
//  MatBoobz
//
//  Created by Kirill Simagin on 21/11/12.
//  Copyright (c) 2012 Kirill Simagin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
