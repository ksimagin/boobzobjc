//
//  BoobzView.m
//  MatBoobz
//
//  Created by Kirill Simagin on 21/11/12.
//  Copyright (c) 2012 Kirill Simagin. All rights reserved.
//

#import "BoobzView.h"

@implementation BoobzView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/**/
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{

    //// Color Declarations
    UIColor* pinkColor = [UIColor colorWithRed: 1 green: 0.469 blue: 0.504 alpha: 1];
    UIColor* color = [UIColor colorWithRed: 1 green: 0.727 blue: 0.695 alpha: 1];
    
    //// Frames
    CGRect frame = rect;//CGRectMake(11, 203, 140, 75);
    

    //// Subframes
    CGRect rightBFrame = CGRectMake(CGRectGetMinX(frame) + 0.95 * frame.size.height, CGRectGetMinY(frame), frame.size.height * 0.9, frame.size.height * 0.9);
    CGRect leftBFrame = CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), frame.size.height * 0.9, frame.size.height * 0.9);
    
    float boobzRad = rightBFrame.size.width;
    //// RightBigBoobz Drawing
    UIBezierPath* rightBigBoobzPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(rightBFrame), CGRectGetMinY(rightBFrame) , boobzRad, boobzRad)];
    [color setFill];
    [rightBigBoobzPath fill];
    
    float aureoleRad = frame.size.height * 0.4;
    //// AureoleRight Drawing
    UIBezierPath* aureoleRightPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(rightBFrame) + rightBFrame.size.height * 0.26, CGRectGetMinY(rightBFrame) + rightBFrame.size.height * 0.26, aureoleRad, aureoleRad)];
    [pinkColor setFill];
    [aureoleRightPath fill];
    
    float nippleRad = frame.size.height * 0.16;
    //// RightPinkyNipple Drawing
    UIBezierPath* rightPinkyNipplePath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(rightBFrame) + rightBFrame.size.height * 0.4, CGRectGetMinY(rightBFrame) + rightBFrame.size.height * 0.4, nippleRad, nippleRad)];
    [[UIColor redColor] setFill];
    [rightPinkyNipplePath fill];
    
    
    //// LeftBigBoobz Drawing
    UIBezierPath* leftBigBoobzPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(leftBFrame) , CGRectGetMinY(leftBFrame) , boobzRad, boobzRad)];
    [color setFill];
    [leftBigBoobzPath fill];
    
    
    //// AureoleLeft Drawing
    UIBezierPath* aureoleLeftPath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(leftBFrame) + leftBFrame.size.height * 0.26, CGRectGetMinY(leftBFrame) + leftBFrame.size.height * 0.26, aureoleRad, aureoleRad)];
    [pinkColor setFill];
    [aureoleLeftPath fill];
    
    
    //// LeftPinkyNipple Drawing
    UIBezierPath* leftPinkyNipplePath = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(CGRectGetMinX(leftBFrame) + leftBFrame.size.height * 0.4, CGRectGetMinY(leftBFrame) + leftBFrame.size.height * 0.4, nippleRad, nippleRad)];
    [[UIColor redColor] setFill];
    [leftPinkyNipplePath fill];
}


@end
